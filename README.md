# OpenFOAM

OpenFOAM をWindows WSL2環境(正確にはWSL2上のLinux環境)に構築して、授業・卒業研究などで活用するための準備資料です。

## CONTENTS

[[_TOC_]]

## WSL2環境構築の構築（WSL2 + Ubuntu 22.04 or 24.04 LTS）
 
### Windows 11

1. 管理者権限でのWindowsターミナルまたは Windows コマンド プロンプトを起動します。  
2. インストール可能なLinuxディストリビューションを確認（ここではOpenFOAMで公式にサポートしているUbuntu 24.04LTSを利用）します。  
```
wsl --list --online

```
<img src="./image/top_01.png"  width="1024">  


3. 名前NAMEを確認したら、プロンプトで以下のようにUbuntu 24.04を指定(-dオプション)してインストールします。
```
wsl --install -d Ubuntu-24.04
```
```
インストール中: Ubuntu-24.04
[========     27.0%                ]
Ubuntu 24.04 LTS がインストールされました。
Ubuntu 24.04 LTS を起動しています...
Installing, this may take a few minutes...
Please create a default UNIX user account. The username does not need to match your Windows username.
For more information visit: https://aka.ms/wslusers
Enter new UNIX username: user
New password:
Retype new password:
passwd: password updated successfully
Installation successful!
  :
要求された操作は正常に終了しました。変更を有効にするには、システムを再起動する必要があります。
```
上記のように表示されたら、Windowsを再起動します。再起動すると下記のような画面が表示され、インストールが続行されます。任意のユーザとパスワードを設定してください。

【参考】 [Microsoft公式HP：WSL のインストール](https://docs.microsoft.com/ja-jp/windows/wsl/install)

### Windows 10

1. Windwos10 May2020(バージョン2004) からは、WSL2（Windows Subsystem for Linux Ver.2）が正式に利用できるようになりました。WSLに比べるとDisk I/Oが大きく改善されCFD計算で利用しても、ネイティブLinuxマシンとそれほど遜色ないようです。こちらをInstallして行きます。  
2. WSLの有効化
    - 画面左下のWindowsアイコンを右クリックして「設定」を実行
    - 「アプリと機能」の中の関連設定から「プログラムと機能」を選択
    - 「Windowsの機能の有効化または無効化」を選択　
    - 「Linux 用 Windows サブシステム」と「仮想マシンプラットフォーム」の項目をチェック☑し 「ＯＫ」 して再起動します。  
3. WSL2をデフォルトバージョンに設定(WSL1とWSL2があります)
スタートメニューを右クリックして、Powershellを管理者モードで起動し、下記コマンドを実行して下さい。

```
wsl --set-default-version 2
```

この際、カーネルコンポーネントの更新が必要な旨、メッセージが出た場合は、[こちら](https://docs.microsoft.com/ja-jp/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)を参考に、カーネル更新プログラムパッケージを先にインストールしてから設定しなおしてください。  
4. Linuxディストリビューションのインストール  
Microsoft storeアプリを開き、希望する Linux ディストリビューションを選択します。ここではUbuntu 20.04LTSをインストールしてください（同様のUbuntuというものもありますが、バージョン付きのほうが何かと管理しやすいです。
  
【参考】 [Microsoft公式HP：以前のバージョンのWSLの手動インストール](https://docs.microsoft.com/ja-jp/windows/wsl/install)

## mobaXtermのインストール

- WSL2上のUbuntu環境では、GUIアプリケーションを使うために[mobaXterm](https://mobaxterm.mobatek.net/)や[VcXsrv](https://sourceforge.net/projects/vcxsrv/),[Xming](http://www.straightrunning.com/XmingNotes/)などのXサーバソフトウェアをWindows側にインストールする必要があります。ここでは、sshクライアントにもなり、お手軽で便利なmobaXtermを導入します。
- [mobaXterm](https://mobaxterm.mobatek.net/)は、無料版のHome editionを利用します。さらにPortable版（USBメモリ等にいれておいてどこでも使える）とInstaller版がありますが、通常は後者でいいでしょう。
- ダウンロードしたHome edtionのInstaller版zipファイルを展開し、インストールしてください（msiインストーラを起動）。
- mobaXtermを起動し、Session画面を開きます。一番右のWSLを選択し「OK」を押してください。

<img src="./image/mobaXterm_settings.png"  width="1024">  

## Windows側とのファイルのやり取りのための準備
WSL上のUbuntuと、Windows側でファイルをやり取りしたいことがあります。
Windows側のExplorerを起動すると、Linuxシステムが見えていると思います。ユーザーフォルダは以下になります。

\wsl.localhost\Ubuntu-24.04\home\<username>

## OpenFOAMのインストール（ESI版OpenFOAM-v2406）

ソースからのインストールも可能ですが、ここではお手軽にESIの[サイト](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/)に掲載されているprecompiledパッケージインストールをWSL-Ubuntuにおいて行います。

```
# Add the repository
curl https://dl.openfoam.com/add-debian-repo.sh | sudo bash

# Install preferred package. Eg,
sudo apt-get install openfoam2406-default

# !! ここで unable to locate .... のようなエラーが出た場合は、先に以下のca-certificatesをインストールしてみてください。 !!!
sudo apt-get install ca-certificates

# OpenFoamの環境変数設定を行って、実行環境になるためには以下の入力を実行します。端末の起動毎に必要です。
# Use the openfoam shell session. Eg,
openfoam2406
```

## 可視化用ソフトウェアParaviewのインストール(Ubuntu)

まずWSL上でXアプリケーションを起動するために、以下の設定を.bashrcに追記します。
(2022/11/29追記)　以下の設定は、PCによっては追記不要の可能性があります。
DISPLAYが見つからない旨のエラーが出る場合は、記述を削除し、再ログインしてから試してみて下さい。
```
export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0.0
unset LIBGL_ALWAYS_INDIRECT
```
paraviewをインストール
```
sudo apt install paraview
```
paraviewの起動
```
paraview &

（2022/11/29追記）
paraview: error while loading shared libraries: libQt5Core.so.5: cannot open shared object file: No such file or directory
というエラーが出る場合は、以下のコマンドを1回だけ実行してからparaviewを実行してみてください。
sudo strip --remove-section=.note.ABI-tag /usr/lib/x86_64-linux-gnu/libQt5Core.so.5
```

## Paraviewのインストール(Windows)

PCのグラフィックカードによっては、WSLのUbuntuからParaviewをうまく起動できない場合があります。
Windows上のParaviewを利用する分には、そこまで問題になることが少ないため、
上記の”Windows側とのファイルのやり取りのための準備”を参考に、Windows側からWindows版Paraviewを起動して、そのファイルを可視化することが可能です。

https://www.paraview.org/download/


## OpenFOAM tutorials

付属のチュートリアルをコピーしてくるには以下のようにします。
まずは、これらの豊富なチュートリアルを実行してみましょう。
いくつかのチュートリアルの解説を別ページで行います。
(書きかけです...m(_ _)m)

```
cd ~/OpenFOAM/user-v2406  (ここにtutorialフォルダをコピーするとして)
cp -r $FOAM_TUTORIALS .
```

1. メッシュ生成
2. 非圧縮性粘性流
3. 圧縮性粘性流 
4. MultiPhase 
5. 並列計算
6. dynamicMesh
7. 乱流(RAS)
8. 乱流(LES/DNS) 
